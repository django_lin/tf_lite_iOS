//
//  Constant.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/7.
//

import Foundation
struct Constant {
    static let sampleTFLFileURL = "https://gitlab.com/jiechau/tf_dnn_edge/-/raw/main/tflite/model.tflite"
    static let sampleONNXFileURL = "https://gitlab.com/jiechau/pt_dnn_edge/-/raw/main/onnx/model.onnx"
    static let documentFileName = "ai_model"
    static let tfFileType = ".tflite"
    static let onnxFileType = ".onnx"

}
