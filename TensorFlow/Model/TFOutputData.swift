//
//  TFOutputData.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/7.
//

import Foundation

struct OutputData {
    
}

extension OutputData {
    static var dummy: Self {
        return OutputData()
    }
}
