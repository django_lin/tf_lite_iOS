//
//  TFModelFetchClientConfig.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/11.
//

import Foundation

protocol PreTrainedModelSources {
    var path: String? { get }
    var fetchType: PreTrainedModelSource { get }
}

public enum PreTrainedModelSource: PreTrainedModelSources, Equatable {
    case remote(path: String)
    case local
    
    var path: String? {
        if case .remote(let path) = self {
           return path
        }
        return nil
    }
    
    var fetchType: PreTrainedModelSource {
        return self
    }
}

@dynamicMemberLookup
public enum PreTrainedModelType {
    
    case tensor(source: PreTrainedModelSource)
    case onnx(source: PreTrainedModelSource)
    
    var fileName: String {
        return Constant.documentFileName
    }
    
    var typeName: String {
        switch self {
        case .tensor:
            Constant.tfFileType
        case .onnx:
            Constant.onnxFileType
        }
    }

    subscript<T>(dynamicMember member: KeyPath<PreTrainedModelSource, T>) -> T {
        switch self {
        case .onnx(source: let source):
            return source[keyPath: member]
        case .tensor(source: let source):
            return source[keyPath: member]
        }
    }
}


