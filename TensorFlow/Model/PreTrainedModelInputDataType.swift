//
//  PreTrainedModelInputDataType.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/18.
//

import Foundation
import onnxruntime_objc

public protocol PreTrainedModelInputDataType {
    ///  All the input data must be data type.
    ///  We need to convert custom type to binary data format.
    /// - Returns: Data type for tensor flow input
    func data() -> Data
    
    ///  For onnx only [batch, field count, dimention]
    func shape() -> [NSNumber]
    
    ///  For onnx only 
    func elementType() -> ORTTensorElementDataType
}
