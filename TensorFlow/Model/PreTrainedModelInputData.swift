//
//  TFInputData.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/7.
//

import onnxruntime_objc
import Foundation

struct InputData {
    var watermelonAmount: Float
    var appleAmount: Float
    var grapeAmount: Float
}

extension InputData {
    var array: [Float] {
        return [
            watermelonAmount,
            appleAmount,
            grapeAmount
        ]
    }
}

extension InputData {
    // expect output should be $554
    static let dummy: Self = .init(
        watermelonAmount: 5,
        appleAmount: 5,
        grapeAmount: 5
    )
}

extension InputData: PreTrainedModelInputDataType {
    func elementType() -> ORTTensorElementDataType {
        ORTTensorElementDataType.float
    }
    
    
    func shape() -> [NSNumber] {
        let inputShape: [NSNumber] = [
            NSNumber(value: 1),
            NSNumber(value: 3)
        ]
        return inputShape
    }
    
    
    func data() -> Data {
        let inputDataFloat32 = array.compactMap { value in
            Float32(value)
        }
        
        // Convert floats to bytes
        let inputData = inputDataFloat32.withUnsafeBytes { f in
            Data(f)
        }
        
        return inputData
    }
}
