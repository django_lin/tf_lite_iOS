//
//  InterpreterClient.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/7.
//

import Foundation
import TensorFlowLite

extension InterpreterClient {
    
    /// - Returns: instance
    static func tensorflow() -> Self {
        return .init { (path, inputData) in
            // Initialize an interpreter with the model.
            let interpreter = try Interpreter(modelPath: path)
            
            // Allocate memory for the model's input `Tensor`s.
            try interpreter.allocateTensors()
            
            // Copy the input data to the input `Tensor`.
            try interpreter.copy(inputData.data(), toInputAt: 0)
            
            // Run inference by invoking the `Interpreter`.
            try interpreter.invoke()
            
            // Get the output `Tensor`
            let outputTensor = try interpreter.output(at: 0)
            
            return outputTensor.data
        }
    }
    
    
}
