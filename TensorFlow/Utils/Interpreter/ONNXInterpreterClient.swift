//
//  ONNXInterpreterClient.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/18.
//

import Foundation
import onnxruntime_objc

extension InterpreterClient {
    
    /// onnx instance
    static func onnx() -> Self {
        return .init { (path, inputData) in
            let ortEnv = try ORTEnv(loggingLevel: ORTLoggingLevel.warning)
            let ortSession = try ORTSession(env: ortEnv,
                                            modelPath: path,
                                            sessionOptions: nil)
            
            let input = try ORTValue(tensorData: NSMutableData(data: inputData.data()),
                                     elementType: inputData.elementType(),
                                     shape: inputData.shape())
            
            let inputName = try ortSession.inputNames().first ?? ""
            let outputName = try ortSession.outputNames().first ?? ""

            let outputs = try ortSession.run(withInputs: [inputName: input],
                                             outputNames: [outputName],
                                             runOptions: nil)
            

            guard let output = outputs[outputName],
                  let outputData = try? output.tensorData() as Data else {
                throw TrainerError.modelInferenceFailed("Failed to get model output from inference.")
            }
            return outputData
        }
    }
}
