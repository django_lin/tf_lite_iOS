//
//  InterpreterClient.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/18.
//

import Foundation

enum TrainerError: Error {
    case modelFileNotFound
    case modelInferenceFailed(String)
}

public struct InterpreterClient<I>
where I: PreTrainedModelInputDataType
{
    typealias Parameter = (path: String, input: I)
    var process: (Parameter) throws -> Data
}
