//
//  TFHubClient.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/6.
//

import Foundation
import Combine

public struct PreTrainedModelFetchClient {
    /// return path
    var download: AnyPublisher<String, Error>?
    var progress: AnyPublisher<Double, Never>
}

public extension PreTrainedModelFetchClient {
    
    static func live(type: PreTrainedModelType) -> Self {
        if type.fetchType == .local {
            return .local(type: type)
        }
        return .remote(type: type)
    }
    
    /// Load  .tflite  or . onnx file from remote url
    static func remote(type: PreTrainedModelType) -> Self {
        let modelURL = URL(string: type.path ?? "")!
        let subject = PassthroughSubject<String, Error>()
        
        // Prpare destination path
        let destinationURL = FileManager.default
            .urls(for: .documentDirectory, in: .userDomainMask)
            .first!
            .appendingPathComponent(type.fileName + type.typeName)
        
        // Init download task with PassthroughSubject
        let task = URLSession.shared.downloadTask(with: modelURL) { localURL, _, error in
            if let localURL = localURL, error == nil {
                do {
                    // Check file exist or not
                    if FileManager.default.fileExists(atPath: destinationURL.path) {
                        try FileManager.default.removeItem(atPath: destinationURL.path)
                    }
                    // Move download file to destination
                    try FileManager.default.moveItem(atPath: localURL.path, toPath: destinationURL.path)
                    
                    // Handle path remove the prefix part for interpreter load needed
                    let path = destinationURL.absoluteString.replacingOccurrences(of: "file:///", with: "")
                    
                    // Emit event
                    subject.send(path)
                    subject.send(completion: .finished)
                    print("Model downloaded successfully at: \(destinationURL)")
                } catch {
                    // Emit fail completion
                    subject.send(completion: .failure(error))
                    print("Error moving model file: \(error)")
                }
            } else {
                print("Error downloading model: \(error?.localizedDescription ?? "Unknown error")")
            }
        }
        
        // Output task's progress
        let progress = task.progress
            .publisher(for: \.fractionCompleted)
            .eraseToAnyPublisher()
        
        return .init(
            download: {
                return subject
                    .handleEvents(
                        receiveSubscription: { _ in task.resume() },
                        receiveCancel: { task.cancel() }
                    )
                    .eraseToAnyPublisher()
            }(),
            progress: progress
        )
    }
    
    
    /// Progress always return 100% and load .tflite  or . onnx  from main bundle
    static func local(type: PreTrainedModelType) -> Self {
        .init(
            download: {
                return Just<String>({
                    guard let modelPath = Bundle.main.path(
                        forResource: type.fileName,
                        ofType: type.typeName
                    ) else { return "Load path from main bundle fail" }
                    return modelPath
                }())
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
            }(),
            progress: Just<Double>(1).eraseToAnyPublisher()
        )
    }
}

