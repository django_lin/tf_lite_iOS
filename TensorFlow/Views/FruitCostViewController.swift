//
//  FruitCostViewController.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/11.
//

import UIKit
import Combine

class FruitCostViewController: UIViewController {
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var caculatePanel: UIStackView!
    @IBOutlet weak var watermelonField: UITextField!
    @IBOutlet weak var appleField: UITextField!
    @IBOutlet weak var grapeField: UITextField!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var resaultLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var modeSwitch: UISwitch!
    @IBAction func switchValueChange(_ sender: UISwitch) {
        viewModel.input.modeChange(tensorFlowIsOn: sender.isOn)
    }
    
    let viewModel: FruitCostViewModel
    private(set) var cancellables: Set<AnyCancellable> = .init()
    
    init(viewModel: FruitCostViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binding()
        viewModel.input.viewDidLoad()
    }
    
    fileprivate func binding() {
        
        // MARK: - Input
        // Binding calculate button send fruit amount to interpreter
        calculateButton.publisher(for: .touchUpInside)
            .sink(receiveValue: { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.input.calculateButtonTap(
                    watermelonAmount: self.watermelonField.float,
                    appleAmount: self.appleField.float,
                    grapeAmount: self.grapeField.float
                )
            })
            .store(in: &cancellables)
        
        modeSwitch.publisher(for: \.isOn, options: .init(arrayLiteral: [.initial, .new]))
            .sink { [weak self] value in
                guard let self = self else { return }
                self.viewModel.input.modeChange(tensorFlowIsOn: value)
            }
            .store(in: &cancellables)
        
        // MARK: - Output
        // Binding resault string
        viewModel.output.$resault
            .sink { [weak self] value in
                guard let self = self else { return }
                guard !value.isEmpty else {
                    self.resaultLabel.text = ""
                    return
                }
                self.resaultLabel.text = "Resault:\nThe price will be $\(value)"
            }
            .store(in: &cancellables)
        
        // Binding progress
        viewModel.output.$progress
            .receive(on: DispatchQueue.main)
            .sink { [weak self] percentage in
                guard let self = self else { return }
                defer {
                    self.calculateButton.isEnabled = percentage >= 1
                }
                self.progressLabel.text = percentage < 1 ? String(format: "%.0f", percentage * 100) + " %" : "Download model file completed"
                self.progressBar.progress = Float(percentage)
            }
            .store(in: &cancellables)
    }
    
}
