//
//  FruitCostViewModel.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/11.
//

import Foundation
import Combine

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    var input: Input { get }
    var output: Output { get }
}

class FruitCostViewModel: ViewModelType {
    class Input {
        fileprivate var viewDidLoadSubject: PassthroughSubject<Void, Never> = .init()
        public func viewDidLoad() {
            viewDidLoadSubject.send(())
        }
        
        fileprivate var calculateButtonTapSubject: PassthroughSubject<InputData, Never> = .init()
        public func calculateButtonTap(
            watermelonAmount: Float,
            appleAmount: Float,
            grapeAmount: Float
        ) {
            calculateButtonTapSubject.send(
                .init(
                    watermelonAmount: watermelonAmount,
                    appleAmount: appleAmount,
                    grapeAmount: grapeAmount
                )
            )
        }
        
        fileprivate var modeChangeSubject: PassthroughSubject<Bool, Never> = .init()
        public func modeChange(tensorFlowIsOn: Bool) {
            modeChangeSubject.send(tensorFlowIsOn)
        }
        
    }
    
    class Output {
        @Published var progress: Double = 0
        @Published var resault: String = ""
    }
    
    var input = Input()
    var output = Output()
    var filePath: String = ""
    
    private(set) var cancellables: Set<AnyCancellable> = .init()
    var progress: AnyCancellable?
    var fetchClient: PreTrainedModelFetchClient
    var interpreterClient: InterpreterClient<InputData>
    
    init(
        fetchClient: PreTrainedModelFetchClient,
        interpreterClient: InterpreterClient<InputData>
    ) {
        self.fetchClient = fetchClient
        self.interpreterClient = interpreterClient
        
        // Handle `calculateButton` tap event
        input.calculateButtonTapSubject.sink { [weak self] data in
            guard let self = self else { return }
            
            let data = try! self.interpreterClient.process(
                (
                    path: self.filePath,
                    input: data
                )
            )
            self.output.resault = "\(data.floatArray.first ?? -999)"
        }
        .store(in: &cancellables)
        
        // Switch source
        let modeSubject = input.modeChangeSubject.map { [weak self] tensorIsOn in
            guard let self = self else { return }
            if tensorIsOn {
                self.fetchClient = .live(type: .tensor(source: .remote(path: Constant.sampleTFLFileURL)))
                self.interpreterClient = .tensorflow()
            } else {
                self.fetchClient = .live(type: .onnx(source: .remote(path: Constant.sampleONNXFileURL)))
                self.interpreterClient = .onnx()
            }
            self.progress = self.progressCancellable()
            return
        }
        
        // Merge with viewDidLoad
        input.viewDidLoadSubject.merge(with: modeSubject)
        .flatMap { [unowned self] _ in
            return self.fetchClient.download!
        }
        .sink(receiveCompletion: { completion in
            print(completion)
        }, receiveValue: { [weak self] path in
            guard let self = self else { return }
            self.filePath = path
        })
        .store(in: &cancellables)
        
        
        
    }
    
    /// Track download progress
    fileprivate func progressCancellable() -> AnyCancellable {
        return fetchClient.progress.sink { [weak self] progress in
            guard let self = self else { return }
            self.output.progress = progress
        }
    }
}
