//
//  Float+extention.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/11.
//

import Foundation
import UIKit

extension UITextField {
    var float: Float {
        let string = text ?? ""
        return (string as NSString).floatValue
    }
}
