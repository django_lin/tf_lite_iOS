//
//  UIViewController+keyBoardDismiss.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/11.
//

import Foundation
import UIKit

extension UIViewController {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.window?.endEditing(true)
        super.touchesEnded(touches, with: event)
    }
}
