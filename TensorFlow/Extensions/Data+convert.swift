//
//  Data+convert.swift
//  TensorFlow
//
//  Created by Django Lin on 2024/6/18.
//

import Foundation

extension Data {
    var floatArray: [Float] {
        let results = self.withUnsafeBytes { (pointer: UnsafeRawBufferPointer) -> [Float] in
            let bufferPointer = pointer.bindMemory(to: Float.self)
            return Array(bufferPointer)
        }
        return results
    }
}
