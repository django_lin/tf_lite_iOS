![from repo](img/screenshot.PNG)

## About this demo
This project with pre-trained model, you can simply provide it with quantities, and it will calculate the total amount. For example, if you ask it for 5 watermelons + 5 apples + 5 grapes, it can calculate $555.


This project demonstrates two ways to fetch a model. To switch between local or remote fetching, simply change the source `.remote` to `.local` in `SceneDelegate`.
```swift
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        //....
        let source: PreTrainedModelSource = .remote(
            path: Constant.sampleONNXFileURL
        )
        //....
    }
```

There two kind of service 
1. Tensor flow lite
2. ONNX

To switch between 1, 2 

### tensor flow
```swift
    let source: PreTrainedModelSource = .remote(
            path: Constant.sampleTFLFileURL
    )
        
    let serviceType: PreTrainedModelType = .tensor(
            source: source
    )
        
    let viewModel = FruitCostViewModel(
            fetchClient: .live(
                type: serviceType
            ),
            interpreterClient: .tensorflow()
    )
```

### onnx
```swift
    let source: PreTrainedModelSource = .remote(
            path: Constant.sampleONNXFileURL
    )
        
    let serviceType: PreTrainedModelType = .onnx(
            source: source
    )
        
    let viewModel = FruitCostViewModel(
            fetchClient: .live(
                type: serviceType
            ),
            interpreterClient: .onnx()
    )
```

## Start 
- cd to `TensorFlow/`
- run `pod install`
- open `TensorFlow.xcworkspace` build and run
**limit only work on real device**


## Tensor flow vs Tensor flow lite

Tensor flow :
Open-source machine learning library designed for building, training, and deploying machine learning models across various platforms including desktop, servers, and mobile devices.

Tensor flow lite :

**Lightweight version optimized for mobile and embedded devices**, aiming to deploy machine learning models on resource-constrained devices

## Model
model format .tflite**

(model can have metadata that has human-readable model description and machine-readable data)

- **Use an existing TensorFlow Lite model**
- **Create a TensorFlow Lite model**
- **Convert a TensorFlow model into a TensorFlow Lite model**

## **Inference**

process of executing a TensorFlow Lite model on-device to make predictions based on input data. 

- Model without metadata:
 [TensorFlow Lite Interpreter](https://www.tensorflow.org/lite/guide/inference) API
- Model with metadata:
 [TensorFlow Lite Task Library](https://www.tensorflow.org/lite/inference_with_metadata/task_library/overview) or build custom inference pipelines with the [TensorFlow Lite Support Library](https://www.tensorflow.org/lite/inference_with_metadata/lite_support).

## Acceleration

iOS: [GPU Delegate](https://www.tensorflow.org/lite/performance/gpu), [Core ML Delegate](https://www.tensorflow.org/lite/performance/coreml_delegate).

Android: [NNAPI Delegate](https://www.tensorflow.org/lite/android/delegates/nnapi),  [Hexagon Delegate](https://www.tensorflow.org/lite/android/delegates/hexagon) (on older devices)

## ONNX

objc runtime api: [document](https://onnxruntime.ai/docs/api/objectivec/index.html)